# P12Mapas

App movil sobre Google Maps

Paso 1. Crear un proyecto Empty
Paso 2. Creamos el layout.
Paso 3. Añadimos en Java/com.example.p12mapas > New > Google > Google Maps Activity
Paso 4. Vamos a https://console.developers.google.com
Paso 5. Click en "Crear" o "Create"
Paso 6. Asignamos nombre y organizacion, depende de la cuenta de google.
Paso 7. > "Library" o "Biblioteca" https://console.developers.google.com/apis/library?authuser=1&project=sodium-keel-240201&supportedpurview=project
Paso 8. Seleccionamos Maps SDK for Android
Paso 9. Habilitamos el API
Paso 10. Lo demas aqui https://developers.google.com/maps/documentation/android-sdk/start
